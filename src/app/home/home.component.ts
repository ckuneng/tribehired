import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	list = [];

	constructor(private http: HttpClient) { }

	ngOnInit() {
		this.http.get<any>('https://jsonplaceholder.typicode.com/posts')
			.subscribe(data => {
				this.list = data;
			})
	}
}
