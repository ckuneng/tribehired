import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'commentFilter'
})
export class CommentFilterPipe implements PipeTransform {

	transform(items: any[], searchStr: string): any {
		console.log(searchStr);
		if (!items || !searchStr) {
			return items;
		}
		return items.filter(item => item.name.toLowerCase().includes(searchStr.toLowerCase()) || item.email.toLowerCase().includes(searchStr.toLowerCase()) || item.body.toLowerCase().includes(searchStr.toLowerCase()));
	}

}
