import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-post',
	templateUrl: './post.component.html',
	styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

	search;
	post = {};
	comment = [];

	constructor(private http: HttpClient, private route: ActivatedRoute) { }

	ngOnInit() {
		this.http.get<any>('https://jsonplaceholder.typicode.com/posts/' + this.route.snapshot.params.id)
			.subscribe(data => {
				this.post = data;
			});

		this.http.get<any>('https://jsonplaceholder.typicode.com/comments?postId=' + this.route.snapshot.params.id)
			.subscribe(data => {
				this.comment = data;
			})
	}

}
